DROP DATABASE IF EXISTS DOC;
CREATE DATABASE DOC;
USE DOC;
DROP TABLE IF EXISTS `doc`.`documents`;
DROP TABLE IF EXISTS `doc`.`roles`;
DROP TABLE IF EXISTS `doc`.`users`;
DROP TABLE IF EXISTS `doc`.`user_roles`;


CREATE TABLE `documents` (
	`Id` INT(3) NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(25) NOT NULL,
	`Author` VARCHAR(25) NOT NULL,
	`Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`Info` VARCHAR(25) NOT NULL,
	`File` VARCHAR(25) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO DOCUMENTS(Name, Author, Date, Info, File) VALUES('Docs1', 'Elena', '2017-10-21 18:00:58', 'info about docs', '/docs1.txt');
INSERT INTO DOCUMENTS(Name, Author, Date, Info, File) VALUES('Docs2', 'Serg', '2017-10-22 18:00:58', 'info about docs2', '/docs2.txt');
INSERT INTO DOCUMENTS(Name, Author, Date, Info, File) VALUES('Docs3', 'Matis', '2017-10-23 18:00:58', 'info about docs3', '/docs3.txt');

CREATE TABLE `roles` (
 	  `id` int(3) NOT NULL AUTO_INCREMENT,
 	  `role` varchar(20) NOT NULL,
 	  PRIMARY KEY (`id`)
 	) ENGINE=InnoDB AUTO_INCREMENT DEFAULT CHARSET=utf8;



CREATE TABLE `users` (
 	  `id` int(3) NOT NULL AUTO_INCREMENT,
 	  `login` varchar(20) NOT NULL,
 	  `password` varchar(20) NOT NULL,
 	  PRIMARY KEY (`id`)
 	) ENGINE=InnoDB AUTO_INCREMENT DEFAULT CHARSET=utf8;



CREATE TABLE `user_roles` (
 	  `user_id` int(3) NOT NULL,
 	  `role_id` int(3) NOT NULL,
 	  KEY `user` (`user_id`),
 	  KEY `role` (`role_id`)
 	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 	INSERT INTO roles (role) VALUES ('admin'), ('user');
 	INSERT INTO users (login, password) VALUES ('admin', 'admin'), ('user', 'user');
 	INSERT INTO user_roles (user_id, role_id) VALUES (1, 1), (2, 2);