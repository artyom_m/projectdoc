package com.artyom.docmanager.controller;

import com.artyom.docmanager.model.Doc;
import com.artyom.docmanager.service.DocService;
import com.artyom.docmanager.service.DocServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@Controller
@RequestMapping("/")
public class DocController {

    private DocService docService;

    @Autowired(required = true)
    @Qualifier("docService")
    public void setDocService(DocService docService) {
        this.docService = docService;
    }


    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public String getDoc(Model model) {
        List<Doc> doc = docService.getAll();
        model.addAttribute("doc", doc);
        return "alldoc";
    }

    @RequestMapping(value = "/searchName", method = RequestMethod.POST)
    public String searchByName(@RequestParam(value="nameDoc", required = true) String name, Model model) {
        List<Doc> doc = docService.searchDocByName(name);
        model.addAttribute("doc", doc);
        return "alldoc";
    }
    @RequestMapping(value = "/searchAuthor", method = RequestMethod.POST)
    public String searchByAuthor(@RequestParam(value="authorDoc", required = true) String author, Model model) {
        List<Doc> doc = docService.searchDocByAuthor(author);
        model.addAttribute("doc", doc);
        return "alldoc";
    }


    @RequestMapping(value="/add", method = RequestMethod.GET)
    public  String getAddDoc(Model model) {
        model.addAttribute("docAttribute", new Doc());
    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
        String username = ((UserDetails)principal).getUsername();
        model.addAttribute("username", username);
    } else {
        String username = principal.toString();
        model.addAttribute("username", username);
    }
        return "add";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addDoc(@Valid  @ModelAttribute("docAttribute") Doc doc, BindingResult result, @RequestParam(value = "file") MultipartFile file, Model model) {
     if(result.hasErrors()) {
        return "add";
    }
        doc.setFileDoc(file.getOriginalFilename());
     model.addAttribute("filename", file.getOriginalFilename());
        docService.add(doc);
        String name = null;


        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                name = file.getOriginalFilename();

                String rootPath = "C:\\Program Files\\apache-tomcat\\webapps\\projectDoc\\resources\\";
                File dir = new File(rootPath + File.separator + "loadFiles");

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                File uploadedFile = new File(dir.getAbsolutePath() + File.separator + name);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                stream.flush();
                stream.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("hi");
        }




        return "/added";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value="id", required = true) Integer id, Model model){
        docService.delete(id);
        model.addAttribute("id", id);
        return "delete";
    }

    @RequestMapping(value="/edit", method = RequestMethod.GET)
    public  String getEditDoc(@RequestParam(value="id", required = true) Integer id, Model model){
        model.addAttribute("docAttribute", docService.get(id));

        return "edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String saveEditedDoc(@Valid @ModelAttribute("docAttribute") Doc doc, @RequestParam(value="id", required=true) Integer id, Model model) {
        doc.setId(id);
        docService.edit(doc);
        model.addAttribute("id", id);
        return "/edited";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model){
        return "login";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model){
        return "index";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String error(Model model){
        return "error";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String tologin(Model model){
        return "login";
    }



}
