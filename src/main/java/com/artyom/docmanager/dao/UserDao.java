package com.artyom.docmanager.dao;

import com.artyom.docmanager.model.User;

public interface UserDao {
    public User getUser(String login);
}
