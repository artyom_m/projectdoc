package com.artyom.docmanager.dao;

import com.artyom.docmanager.model.Doc;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class DocDaoImpl implements DocDao {


    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    public List<Doc> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from doc t ORDER BY t.nameDoc ASC, t.authorDoc ASC, t.dateDoc ASC, t.infoDoc ASC");
        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<Doc> searchDocByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM doc t WHERE t.nameDoc like  :inName ORDER BY t.nameDoc ASC");
        query.setParameter("inName","%"+ name + "%");
        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<Doc> searchDocByAuthor(String author){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM doc t WHERE t.authorDoc like :inAuthor ORDER BY t.authorDoc ASC");
        query.setParameter("inAuthor", "%"+ author +"%");
        return query.list();
    }


        public Doc get(Integer id){
            Session session = sessionFactory.getCurrentSession();
            Doc doc = (Doc) session.get(Doc.class, id);
            return doc;
        }

        public void add(Doc doc){
            Session session = sessionFactory.getCurrentSession();

            session.save(doc);
        }

        public void delete(Integer id){
            Session session = sessionFactory.getCurrentSession();
            Doc doc = (Doc) session.get(Doc.class, id);
            if (doc != null)
                session.delete(doc);

        }

        public void edit(Doc doc) {
            Session session = sessionFactory.getCurrentSession();
            Doc editDoc = (Doc) session.get(Doc.class, doc.getId());
            editDoc.setNameDoc(doc.getNameDoc());
            editDoc.setAuthorDoc(doc.getAuthorDoc());
            editDoc.setInfoDoc(doc.getInfoDoc());
            editDoc.setFileDoc(doc.getFileDoc());
            editDoc.setDateDoc(doc.getDateDoc());

            session.save(editDoc);

        }

}
