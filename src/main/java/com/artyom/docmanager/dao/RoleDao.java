package com.artyom.docmanager.dao;

import com.artyom.docmanager.model.Role;

public interface RoleDao {
    public Role getRole(int id);
}
