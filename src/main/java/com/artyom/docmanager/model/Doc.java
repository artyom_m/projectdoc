package com.artyom.docmanager.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity(name="doc")
@Table(name="DOCUMENTS")
public class Doc implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "NAME")
    @Size(min=4, max=10)
    private String nameDoc;

    @Column(name = "DATE")
    private Timestamp dateDoc;

    @Column(name = "AUTHOR")
    @Size(min=4, max=10)
    private String authorDoc;



    @Column(name = "FILE")
    private String fileDoc;

    @Size(min=1, max=100)
    @Column(name = "INFO")
    private String infoDoc;

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getNameDoc(){
        return nameDoc;
    }

    public void setNameDoc(String nameDoc){
        this.nameDoc = nameDoc;
    }

    public Timestamp getDateDoc(){
        return dateDoc;
    }

    public void setDateDoc(Timestamp dateDoc){
        this.dateDoc = dateDoc;
    }

    public String getAuthorDoc(){
        return authorDoc;
    }

    public void setAuthorDoc(String autorDoc){
        this.authorDoc = autorDoc;
    }

    public String getFileDoc(){
        return fileDoc;
    }

    public void setFileDoc(String fileDoc){
        this.fileDoc = fileDoc;
    }

    public String getInfoDoc(){
        return infoDoc;
    }

    public void setInfoDoc(String infoDoc){
        this.infoDoc = infoDoc;
    }

}


