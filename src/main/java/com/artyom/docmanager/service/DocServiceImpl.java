package com.artyom.docmanager.service;

import com.artyom.docmanager.dao.DocDao;
import com.artyom.docmanager.model.Doc;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import javax.persistence.criteria.CriteriaBuilder;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class DocServiceImpl implements DocService{


    private DocDao docDao;

    public void setDocDao(DocDao docDao){
        this.docDao = docDao;
    }

    public List<Doc> getAll() {
        return this.docDao.getAll();
    }

    public List<Doc> searchDocByName(String name){
        return this.docDao.searchDocByName(name);
    }

    public List<Doc> searchDocByAuthor(String author) {
        return this.docDao.searchDocByAuthor(author);
    }

    @Transactional
    public Doc get(Integer id){
            return this.docDao.get(id);
    }

    @Transactional
    public void add(Doc doc){
           this.docDao.add(doc);
    }

    @Transactional
    public void delete(Integer id){
          this.docDao.delete(id);
    }

    @Transactional
    public void edit(Doc doc) {
         this.docDao.edit(doc);
    }

}
