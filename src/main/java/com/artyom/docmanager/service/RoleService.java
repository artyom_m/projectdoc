package com.artyom.docmanager.service;

import com.artyom.docmanager.model.Role;

public interface RoleService {
    public Role getRole(int id);
}
