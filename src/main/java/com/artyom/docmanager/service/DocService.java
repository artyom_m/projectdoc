package com.artyom.docmanager.service;

import com.artyom.docmanager.model.Doc;

import java.util.List;

public interface DocService {
    public List<Doc> getAll();
    public List<Doc> searchDocByName(String name);
    public List<Doc> searchDocByAuthor(String author);
    public Doc get(Integer id);
    public void add(Doc doc);
    public void delete(Integer id);
    public void edit(Doc doc);
}
