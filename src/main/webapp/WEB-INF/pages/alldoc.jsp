<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DocumentsSaver</title>
    <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.css">
</head>
<body>

<div class="navbar navbar-default">
    <div class ="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">DocAll</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/projectDoc/getall.html">getAll</a></li>
             <li><a href="/projectDoc/add.html">add</a></li>
        </ul>

        <c:url var="nameUrl" value="/searchName"/>
        <c:url var="authorUrl" value="/searchAuthor"/>
                <form class="navbar-form navbar-right" method="POST" action="${nameUrl}">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Names" name="nameDoc">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </form>

                <form class="navbar-form navbar-right" method="POST" action="${authorUrl}">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Authors" name="authorDoc">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </form>
    </div>


</div>
</div>
<div class="container">
    <div class="row">
             <div class="span12">
                 <h3 class="text-center">All docs</h3>
                 <c:url var="addUrl" value="/add" />
                 <table class="table">
                     <thead>
                         <tr>
                             <th>Id</th>
                             <th>Name</th>
                             <th>Author</th>
                             <th>Date</th>
                             <th>Info</th>
                             <th>File</th>
                         </tr>
                     </thead>
                     <tbody>
                        <c:forEach items="${doc}" var="doc">
                            <c:url var="editUrl" value="/edit?id=${doc.id}" />
                            <c:url var="deleteUrl" value="/delete?id=${doc.id}" />
                            <c:set var="fileUrl" value="/projectDoc/resources/loadFiles/${doc.fileDoc}" />

                          <tr>
                            <td><c:out value="${doc.id}" /></td>
                           <td><c:out value="${doc.nameDoc}" /></td>
                           <td><c:out value="${doc.authorDoc}" /></td>
                           <td><c:out value="${doc.dateDoc}" /></td>
                            <td><c:out value="${doc.infoDoc}" /></td>
                            <td><a href="${fileUrl}"><img src="resources/img/file.png" width="30" height="30" alt="Open file"></a></td>

                           <td><a href="${editUrl}">Edit</a></td>
                           <td><a href="${deleteUrl}">Delete</a></td>
                          </tr>
                        </c:forEach>
                     </tbody>
                 </table>
                 <c:if test="${empty doc}">
 No docs. <a href="${addUrl}">Add</a> a doc.
</c:if>
            </div>
         </div>
      </div>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"><!----></script>
<script src="resources/bootstrap/css/bootstrap.js"><!----></script>
</body>
</html>