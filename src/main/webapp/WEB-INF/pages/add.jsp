<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DocumentsSaver</title>
    <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.css">
</head>
<body>

<div class="navbar navbar-default">
    <div class ="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">DocAll</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                 <li><a href="/projectDoc/getall.html">getAll</a></li>
                 <li class="active"><a href="/projectDoc/add.html">add</a></li>
            </ul>
        </div>
    </div>
</div>

<h1 class="text-center">Add document</h1>
<c:url var="saveUrl" value="/add.html" />
    
    
<form:form class="form-horizontal" modelAttribute="docAttribute" method="POST" enctype="multipart/form-data" action="${saveUrl}">

  <div class="form-group">
    <form:label class="control-label col-xs-3" path="nameDoc">Name:</form:label>
    <div class="col-xs-6">
        <form:input type="text" class="form-control" path="nameDoc" placeholder="Enter name(from 3 to 10)"/>
    </div>
    <div class="has-error help-block">
        <form:errors path="nameDoc" class="help-inline"/>
    </div>
  </div>
  
  <div class="form-group">
   <form:label class="control-label col-sm-3" path="authorDoc">Author</form:label>
      <div class="col-sm-6">
        <form:input type="text" class="form-control" path="authorDoc" value="${username}" placeholder="Enter author(from 3 to 10)"/>
      </div>
      <div class="has-error help-block">
         <form:errors path="authorDoc" class="help-inline"/>
      </div>
  </div>
 

   
  <div class="form-group">
   <form:label class="col-sm-3 control-label" path="infoDoc">Info</form:label>
      <div class="col-sm-6">
        <form:input type ="text" class="form-control" path="infoDoc" placeholder="Enter info(from 1 to 100)"/>
      </div>
      <div class="has-error help-block">
                <form:errors path="infoDoc" class="help-inline"/>
      </div>
  </div>

<div class="form-group">
   <label class="col-sm-3 control-label">File</label>
    <div class="col-sm-6">
            <input type="file" class="form-control" name="file" />
    </div>

    <div class="has-error help-block">
        <errors name="fileDoc" class="help-inline"/>
    </div>
  </div>
    
   <div class="form-group">
       <div class="col-xs-offset-3 col-xs-9">
            <button type="submit" class="btn btn-default">Add</button>
        </div>
  </div>
</form:form>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="resources/bootstrap/css/bootstrap.js"></script>
    
</body>
</html>