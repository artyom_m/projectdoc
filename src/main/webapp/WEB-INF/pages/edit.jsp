<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DocumentsSaver</title>
    <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.css">
</head>
<body>
<div class="navbar navbar-default">
    <div class ="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">DocAll</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
             <li><a href="/projectDoc/getall.html">getAll</a></li>
             <li><a href="/projectDoc/add.html">add</a></li>
        </ul>
    </div>

</div>
</div>
<h1 class="text-center">Edit document</h1>
<c:url var="saveUrl" value="/edit?id=${docAttribute.id}" />

<form:form class="form-horizontal" modelAttribute="docAttribute" method="POST" action="${saveUrl}">
    
  <div class="form-group">
    <form:label class="control-label col-xs-2" path="nameDoc">Name: </form:label>
    <div class="col-xs-8">
        <form:input type="text" class="form-control" path="nameDoc" placeholder="Enter name(from 3 to 10)}"/>
    </div>
    <div class="has-error help-block">
            <form:errors path="nameDoc" class="help-inline"/>
        </div>
  </div>
  
  <div class="form-group">
   <form:label class="control-label col-sm-2 " path="authorDoc">Author: </form:label>
      <div class="col-sm-8">
        <form:input type="text" class="form-control" path="authorDoc" placeholder="Enter author(from 3 to 10)"/>
      </div>
      <div class="has-error help-block">
              <form:errors path="authorDoc" class="help-inline"/>
          </div>
  </div>

   
  <div class="form-group">
   <form:label class="col-sm-2 control-label" path="infoDoc">Info: </form:label>
      <div class="col-sm-8">
        <form:input type ="text" class="form-control" path="infoDoc" placeholder="Enter info(from 1 to 100)"/>
      </div>
      <div class="has-error help-block">
              <form:errors path="infoDoc" class="help-inline"/>
          </div>
  </div>

<div class="form-group">
   <label class="col-sm-2 control-label">File</label>
    <div class="col-sm-8">
        <form:input type="text" class="form-control" path="fileDoc" readonly="true"/>
    </div>
    <div class="has-error help-block">
            <form:errors path="fileDoc" class="help-inline"/>
        </div>
  </div>
    
   <div class="form-group">
    <div class="col-xs-offset-2 col-xs-10">
      <button type="submit" class="btn btn-default">Edit</button>
    </div>
  </div>
</form:form>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="resources/bootstrap/css/bootstrap.js"></script>
    
</body>
</html>