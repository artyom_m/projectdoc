<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DocumentsSaver</title>
    <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.css">
</head>
<body>
<div class="container">
    <h1 class="text-center">Log in</h1>
<c:url var="loginUrl" value="/j_spring_security_check" />
<form:form class="form-horizontal" method="POST" action="${loginUrl}">

  <div class="form-group">
   <label class="col-sm-4 control-label" path="username">Login:</label>
      <div class="col-sm-4">
        <input type ="text" class="form-control" name="username"/>
      </div>
  </div>

<div class="form-group">
   <label class="col-sm-4 control-label" path="password">Password:</label>
    <div class="col-sm-4">
        <input type="text" class="form-control" name="password"/>
    </div>
  </div>
    
   <div class="form-group">
    <div class="col-xs-offset-4 col-xs-7">
      <button type="submit" class="btn btn-default">Войти</button>
    </div>
  </div>
</form:form>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="resources/bootstrap/css/bootstrap.js"></script>
    
</body>
</html>