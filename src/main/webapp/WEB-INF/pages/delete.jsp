<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DocumentsSaver</title>
    <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.css">
</head>
<body>

<div class="navbar navbar-default">
    <div class ="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">DocAll</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
             <li><a href="/projectDoc/getall.html">getAll</a></li>
             <li><a href="/projectDoc/add.html">add</a></li>
        </ul>
    </div>

</div>
</div>




<h1 class="text-center">Document deleted!!!</h1>
<img src="resources/img/delete.jpg" alt="" width="220" height="200" class="img-circle center-block" >
<p class="text-center">You deleted document with id ${id} at <%= new java.util.Date() %></p>
 
<c:url var="mainUrl" value="/getall.html" />
<p class="text-center">Return to <a href="${mainUrl}">getAll</a></p>
 
</body>
</html>