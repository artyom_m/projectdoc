<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DocumentsSaver</title>
    <link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.css">
</head>
<body>

<div class="container">
    <h1 class="text-center">Error</h1>
</div>
<img src="resources/img/error.jpg" alt="" width="220" height="200" class="img-circle center-block" >

 
<c:url var="mainUrl" value="/login" />
<p class="text-center">Return to <a href="${mainUrl}">Log in</a></p>
 
</body>
</html>